import React from 'react'
import ListaLugaresItems from '../components/ListaLugaresItems';
import Search from '../components/Search';
import Loading from '../components/Loading';
import firebase from 'firebase';
import { Button } from 'react-bootstrap';
import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';




class Home extends React.Component {
  ////////////////////////////////////////////////////////////////////////////////////
   //Para no abrir la ventana despues de actualizar la carga OBS: debe ser seleccionado el button editar y eliminar
   constructor(props) {
    super(props);

    this.state = { 
      modalShow: false,
      modalShow1: false,
      loading: false,
      error:null,
      page: 0,
      regionsList:[],
      filter:{
        search:'',
        regionName:''
      },
      regionsListBlockCantidad: [],
      regionsListBlock: [],
      cantidad: 50,
      value:{
        search:''
      }
    };
    this.handleLoad=this.handleLoad.bind(this)
    this.handleOnSearch=this.handleOnSearch.bind(this)
    this.handleOnAddRegionsList=this.handleOnAddRegionsList.bind(this)
    this.handleOnEditRegionsList=this.handleOnEditRegionsList.bind(this)
    this.handleOnDeleteRegionsList=this.handleOnDeleteRegionsList.bind(this)


  }

//////////////////////////////////////////////////////////////////////////////////
 componentWillMount(){

  firebase.auth().onAuthStateChanged(user=> {
    if (!user) {
      // console.log(`No tiene sesion en este Home: ${user} `);
      this.props.history.push('/');
    }else{
      // console.log(`Tiene sesion en este Home: ${user.email} `)

    }
  });




  this.setState({loading:true})
 
  
  fetch(
    'https://us-central1-asignacion-tarifas.cloudfunctions.net/getRegionsList/',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({searchValue:this.state.value.search, firstElementsPosition:0}),
      
    }
  ).then(response=>{
    
    if(response.ok){
      return response.json()
    }else{
      throw new Error('Algo salio mal :(');
    }
  })
  .then(resp=>{
    this.setState({regionsList: resp.regionsList,loading:false})
    // console.log(this.state.regionsList)
    // this.handleLoad();
  }).catch(error=>this.setState({error,loading:false}))
 }



//////////////////////////////////////////////////////////////////////////////////////
  //Para agregar objeto dentro arreglo regionList sin la carga
  handleOnAddRegionsList(objeto){
    // console.log(objeto)
    let list = this.state.regionsList
    list.unshift(objeto)
    this.setState({
      regionsList: list
    })
  }

//////////////////////////////////////////////////////////////////////////////////////
  //Para editar objeto dentro arreglo regionList sin la carga
  handleOnEditRegionsList(objetoIndex,objetoEdit){
    // console.log(objetoIndex,objetoEdit)
    let listEdit = this.state.regionsList
    listEdit.splice(objetoIndex,1,objetoEdit)
    this.setState({
      regionsList: listEdit
    })
  }

//////////////////////////////////////////////////////////////////////////////////////
  //Para eliminar objeto dentro arreglo regionList sin la carga
  handleOnDeleteRegionsList(objeto){
    // console.log(objeto)
    let listDelete = this.state.regionsList
    listDelete.splice(objeto,1)
    this.setState({
      regionsList: listDelete
    })
  }

 



 

 //Para mostrar bloque de regiones
 /////////////////////////////////////////////////////////////////////////////////
handleLoad(){
  
  fetch(
    'https://us-central1-asignacion-tarifas.cloudfunctions.net/getRegionsList/',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({searchValue:'', firstElementsPosition:this.state.cantidad}),
      
    }
  ).then(response=>{
    
    if(response.ok){
      return response.json()
    }else{
      throw new Error('Algo salio mal :(');
    }
  })
  .then(resp=>{
    this.setState({regionsList:this.state.regionsList.concat(resp.regionsList)})
    // this.setState({regionsListBlock: resp.regionsList})
  });

  // this.setState({regionsList:this.state.regionsList.concat(this.state.regionsListBlock)})
  
  
   return(
        
      //  this.setState({regionsListBlock:this.state.regionsList.slice(0,this.state.cantidad)}) , 
       this.setState({cantidad:this.state.cantidad + 50}) 
 
    )
}



 //Para buscador
 /////////////////////////////////////////////////////////////////////////////////

 handleOnSearch = e =>{   
  //  console.log(`antes: ${this.state.value.search}`)
   this.setState({ 
     value:{
       ...this.state.value,
       [e.target.name] :e.target.value
      }
    })

    //Este es el asignador de input de busqueda para buscar desde la peticion
    const searchValue = e.target.value
    
   // console.log(`Input: ${searchValue}`)
   

  fetch(
    'https://us-central1-asignacion-tarifas.cloudfunctions.net/getRegionsList/',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({searchValue:searchValue, firstElementsPosition:0}),
      
    }
  ).then(response=>{
    
    if(response.ok){
      return response.json()
    }else{
      throw new Error('Algo salio mal :(');
    }
  })
  .then(resp=>{
    this.setState({regionsList: resp.regionsList})
    // console.log(this.state.regionsList)
    // this.handleLoad();
  });

}

//////////////////////////////////////////////////////////////////////////////////////
componentWillUnmount(){
  // console.log("Salí de la vista de Home")
}

 
/////////////////////////////////////////////////////////////////////////////////////
  render() {

    ////////////////////////////////////////////////////////////
    var searchButton;
      if (this.state.value.search==='') {
        searchButton = <Button variant="secondary" size="mx" block onClick={this.handleLoad} >Cargar más lugares</Button>;
      }


    const {loading, error}=this.state;
    if(error){
      return <p>{error.message}</p>
    }

    if(loading){
      // return 'loading...';
     return <Loading/>
    } 
    return (
        <div>
         <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
            />
            {/* Same as */}
        <ToastContainer />
            {/* <input type="search" id="search" name="search" value={this.state.value.search} onChange={this.onSearch} className="form-control mb-4" placeholder="Buscar Perfil"/> */}
            <Search 
            onSearch={this.handleOnSearch}
            onRegionsListAdd={this.handleOnAddRegionsList}
            /> 
            {/* <ListaLugaresItems data={{filter: this.state.filter, filterResult: this.handleOnFilter(this.state.filter, this.state.regionsList), dataBlock: this.state.regionsListBlock }} />  */}
            {/* <ListaLugaresItems data={this.handleOnFilter(this.state.filter, this.state.regionsList)}/> */}
            <ListaLugaresItems 
            data={this.state.regionsList}
            onRegionsListDelete={this.handleOnDeleteRegionsList}
            onRegionsListEdit={this.handleOnEditRegionsList}
            />
           
            {/* <ListaLugaresItems data={this.state.regionsList}/>             */}
            {searchButton}
            
            

            

            
            
        
        </div> 

    );

    
            
  }
}

export default Home;