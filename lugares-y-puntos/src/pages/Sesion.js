import React from 'react';
import './styles/Sesion.css';
import firebase from 'firebase';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class Sesion extends React.Component {
//////////////////////////////////////////////////////////////////////////////
  constructor(props) {
    super(props);
    
    this.state = {
      email: '',
      password: '',
    };
    
    this.login = this.login.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.signup = this.signup.bind(this);
  }
//////////////////////////////////////////////////////////////////////////////////
  componentWillMount(){
    /* firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log(user)
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        var providerData = user.providerData;
        // ...
      } else {
      //  console.log("No hay una sesión activa")
      }
    }); */
  }

  //////////////////////////////////////////////////////////////////////////////////////
componentWillUnmount(){
  // console.log("Salí de la vista de Sesion")
 /*  firebase.auth().onAuthStateChanged(user=> {
      if(user.email){
        // console.log("Salí de la vista de Sesion")
        toast.success(`Bienvenido ${user.email}!!! `);        
        }
           
        
    } 
  ); */
}

  ///////////////////////////////////////////////////////////////////////////////////////
  //Llamar los valores de Input
  handleInputChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  ///////////////////////////////////////////////////////////////////////////////////
  // Funcion para inicio de sesion de firebase
  login(e) {
    e.preventDefault();
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
    .then(result =>{
      // console.log(`${result.user.email} ha iniciado sesion `);
    if(result.user.email){
      toast.success(`Bienvenido ${result.user.email}!!! `);
      this.props.history.push('/home');
      
      };
    })
    .catch((error) => {
      // console.log(`Correo incorrecta o contraseña incorrecta: ${error} `);
      toast.error(`Correo y Contraseña incorrecta!!!`);
    });
  }


////////////////////////////////////////////////////////////////////////////////////////////
// Funcion para la cuenta nueva de firebase
  signup(e){
    e.preventDefault();
    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
    .then(result =>{
      // console.log(`${result.user.email} ha registrado y ha iniciado sesion `);
    if(result.user.email){
      this.props.history.push('/home');
    };
  })
    .catch((error) => {console.log(error);
        
      })
  }

  render() {
    const { email, password} = this.state;
    return (
      
      <div>
       <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
            />
            {/* Same as */}
        <ToastContainer />
      <div className="Sesion">
     
        <div className="Sesion__header">
          <h4>LOGIN</h4>
        </div>
        <div className="Sesion__section-name">
            
            <form onSubmit={this.handleSubmit}>

            <input type="text" name="email" className="form-control mb-3" placeholder="Ingrese Correo" value={email} onChange={this.handleInputChange}/>

            <input type="password" name="password" className="form-control mb-3" placeholder="Ingrese Password" value={password}
               onChange={this.handleInputChange}/>

            <button type="submit" className="button__inicio btn btn-primary mb-3" onClick={this.login}>Iniciar</button>
            <button onClick={this.signup}  className="button__inicio btn btn-success mb-3">Registrar</button>
            </form>
            {/* <h6>A olvidado contraseña?</h6> */}

        </div>

      </div>
      </div>
    );
  }
}

export default Sesion;