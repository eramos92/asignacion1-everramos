import React from 'react';
import { Button, Modal} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import firebase from 'firebase';

class ModalEliminar extends React.Component {
  constructor(props){
    super(props);
    this.state={
      uid:''
    };

    this.handleDeleteRegister = this.handleDeleteRegister.bind(this);

  }

  ///////////////////////////////////////////////////////////////////////////////

  handleDeleteRegister(){
    
    fetch(
      'https://us-central1-asignacion-tarifas.cloudfunctions.net/removeRegion',
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          codeRegion:this.props.dataDelete.regionCode,
          uid:this.state.uid
        }),
      }
      ).then(response=> response.json())
      .then((json) => json.success)
      .then(success =>{
        // console.log('value-response:', success)
        // console.log(this.state.uid)
        if(success===1){
          // console.log('eliminado')
            toast.success(' Registro esta eliminado!!!');
          }else{
            toast.error(' Error al eliminar registro!!!')
          }
        });

        this.props.onRegionDelete(this.props.dataDeleteIndex)

        this.props.onHide()
  }

/////////////////////////////////////////////////////////////////////////////
componentWillMount(){
  firebase.auth().onAuthStateChanged(user=> {
    if (user) {
      // console.log(user.uid)
      var uid = user.uid;
      this.setState({uid:uid})
    } else {
      // console.log("No hay sesión activa");
    }
  });
}


//////////////////////////////////////////////////////////////////////////////
    render() {
      return (

        <div>
        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
          />
          {/* Same as */}
      <ToastContainer />

        <Modal
          {...this.props}
          size="mx"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              ¿Estás seguro?
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
                Desea Eliminar registro {this.props.dataDelete.regionCode}.           
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.handleDeleteRegister}>Eliminar</Button>
            <Button variant="primary" onClick={this.props.onHide}>Cancel</Button>
          </Modal.Footer>
        </Modal>
        </div>
      );
    }
  }

  export default ModalEliminar;