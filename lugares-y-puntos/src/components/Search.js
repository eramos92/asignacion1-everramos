import React from 'react'
import ModalNuevo from './ModalNuevo';
// import firebase from 'firebase';
import { Button, ButtonToolbar} from 'react-bootstrap';
import './styles/ListaLugares.css';


class Search extends React.Component {

    //Para no abrir la ventana despues de actualizar la carga OBS: debe ser seleccionado el button agregar
    constructor(props) {
      super(props);
  
      this.state = { 
        modalShow: false, 
        regionsList:this.props.data,
        filter:{
          regionName:'',
          search: ''
        }
      };
    }
  
    // Para mostrar la consola si el usuario esta activo o no
    componentWillMount(){
      //   firebase.auth().onAuthStateChanged(function(user) {
      //   if (user) {
      //     //console.log(user)
          
      //     var displayName = user.displayName;
      //     var email = user.email;
      //     var emailVerified = user.emailVerified;
      //     var photoURL = user.photoURL;
      //     var isAnonymous = user.isAnonymous;
      //     var uid = user.uid;
      //     var providerData = user.providerData;
      //     // ...
      //   } else {
      //     // User is signed out.
      //     // ...
      //   }
      // });
    }
  

  
      render() {
        let modalClose = () => this.setState({ modalShow: false });
        //console.log(this.state.regionsList)
          
        //Para cerrar la ventana en X de modal nuevo
  
        return (
        <div className="Lista__container">
          <div className="container">
            <div className="row">
                  <div className="col-10">
                     <input type="search" id="search" value={this.props.value} name="search" onChange={this.props.onSearch} className="form-control mb-4" placeholder="Buscar Perfil"/>
                  </div>
                  <div className="col-2">
                        <ButtonToolbar>
                            <Button
                              variant="success"
                              onClick={() => this.setState({ modalShow: true })}
                              className="btn form-control mr-4"
                            >
                              Agregar
                            </Button>
  
                            <ModalNuevo
                              show={this.state.modalShow}
                              onHide={modalClose}
                              onRegions={this.props.onRegionsListAdd}
                            />
                          </ButtonToolbar>
                  </div>
            </div>
          </div>   
        </div>
        );
        
    }
  } 
  export default Search;
  