import React from 'react';
import './styles/ListaLugares.css';
import Loading from './Loading'
import { Button, ButtonToolbar} from 'react-bootstrap';
import ModalEditar from './ModalEditar';
import ModalEliminar from './ModalEliminar';
import {Link} from 'react-router-dom';




// Listado de datos de lugares
class   ListaLugaresItems extends React.Component {
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  //Para no abrir la ventana despues de actualizar la carga OBS: debe ser seleccionado el button editar y eliminar
  constructor(...props) {
    super(...props);

    this.state = { 
      prueba:true,
      modalShow: false,
      modalShow1: false,
      loading: false,
      error:null,
      regionCode:'',
      codigoPrueba : '',
      regionToEdit: {},
      regionToDetalles:{},
      registerToDelete:{},
      lugar:'',
      registerToDeleteIndex:{},
      registerToEditIndex:{} 

    };
  }





////////////////////////////////////////////////////////////
handleClick(regionCode){
  this.setState({
    regionCode : regionCode
  });
  // console.log(this.state.regionCode)

}


/////////////////////////////////////////////////////////////
  render() {

     //Para cargar loading soinner antes de mostrar a un llamada de sevicio
    const {loading, error}=this.state;

    if(error){
      return <p>{error.message}</p>
    }

    if(loading){
      // return 'loading...';
     return <Loading/>
    } 

    let modalClose = () => this.setState({ modalShow: false });
    let modalClose1 = () => this.setState({ modalShow1: false });
    // console.log(this.state.data)
    return (
      <div  className="Lista__container">
        {/* <Search data={this.state.regionsList}/> */}
        <table className="table" >
          <thead className="thead-dark">  
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Nombre de Colonia</th>
              <th scope="col">E.D.</th>
              <th scope="col">R.N.D.</th>
              <th scope="col">C.P.R.</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
          { this.props.data.map((lugar,index) => {
                  return (
                    <tr key={lugar.regionCode}>
                      <th id="regionCode" scope="row">{lugar.regionCode}</th>
                      <td>{lugar.regionName}</td>
                      <td>{lugar.available}</td>
                      <td>{lugar.radioNotAvailable}</td>
                      <td>{lugar.numberRepresentativePoint}</td>
                      <td> 
                      
                            {/* Para detalles, editar y eliminar */}
                            <ButtonToolbar>
                                        <Link className="btn btn-success mr-1" to={`/home/regionCode=${lugar.regionCode}&regionName=${lugar.regionName}`} 
                                        onClick={()=>{
                                          this.handleClick.bind(this,lugar.regionCode)
                                          this.setState({regionToDetalles:lugar})
                                          }
                                          }
                                        >
                                          Detalle
                                        </Link>
                                                                                                              
                                        <Button
                                          variant="primary"
                                          onClick={() => {
                                            //this.getRegionCode( lugar.regionCode)
                                            this.setState({ modalShow: true, 
                                            regionToEdit: lugar,
                                            registerToEditIndex: index
                                            });
                                            }
                                          }
                                          className="btn btn-primary mr-1"
                                        >
                                          Editar
                                        </Button>

                                        <Button
                                          variant="danger"
                                          onClick={() => this.setState({ modalShow1: true, registerToDelete:lugar, registerToDeleteIndex:index})}
                                          className="btn btn-danger"
                                        >
                                          Eliminar
                                        </Button>

                                      

                            </ButtonToolbar> 
                            </td>
                    </tr>
                  )
                }) }
                </tbody>
  
            
          
        </table>
        <ModalEditar
          show={this.state.modalShow}
          onHide={modalClose}
          data={this.state.regionToEdit}
          onRegionEdit={this.props.onRegionsListEdit}
          dataEditIndex={this.state.registerToEditIndex}
         
        />
        <ModalEliminar
          show={this.state.modalShow1}
          onHide={modalClose1}
          dataDelete={this.state.registerToDelete}
          onRegionDelete={this.props.onRegionsListDelete}
          dataDeleteIndex={this.state.registerToDeleteIndex}
        />
    </div>
    );
  }
}
/////////////////////////////////////////////////////////////////
export default ListaLugaresItems;