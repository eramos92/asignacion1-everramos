import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';


import Sesion from '../pages/Sesion';
import Home from '../pages/Home';
import NotFound from '../pages/NotFound';
//import Detalles from '../pages/Detalles';
import ListaLugaresDetalles from './ListaLugaresDetalles';
//import firebase from 'firebase';


import Layout from './Layout';
// import NotFound from '../pages/NotFound';


function App(){


    return (
        
        <BrowserRouter>
            <Layout>
                <Switch>
                    
                    <Route exact path="/" component={Sesion}/>
                    <Route exact path="/home" component={Home}/>
                    <Route exact path="/home/regionCode=:regionCode&regionName=:regionName" component={ListaLugaresDetalles} />
                    <Route component={NotFound}/>
                </Switch>
             </Layout> 
        </BrowserRouter>
        
    )
}

export default App; 