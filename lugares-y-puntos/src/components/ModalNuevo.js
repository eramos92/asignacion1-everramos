import React from 'react';
import { Button, Modal, Form, Col, Row } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import firebase from 'firebase';
import './styles/UpperCase.css';
//import Loading from './Loading';
//import LoadingOverlay from 'react-loading-overlay';
// import { LoadingOverlay, Loader } from 'react-overlay-loader';
 

class ModalNuevo extends React.Component {

///////////////////////////////////////////////////////////////
constructor(props) {
  super(props);
  this.state = {
    
    uid:'',
    name:'',
    value:{
      regionName:'',
      available: '1',
      radioNotAvailable:'0',
      uid:''
      
    },
    regionsList:{
      regionCode:'',
      regionName:'',
      available:'',
      radioNotAvailable:'',
      numberRepresentativePoint: 0,
      uid:''

    },
    email:'',
    loading:false,

  };

  this.handleChange = this.handleChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
}


/////////////////////////////////////////////////////////////////
  handleChange = e =>{
    console.log(this.state.value.available)   
    this.setState({ 
      value:{
        ...this.state.value,
        [e.target.name] :e.target.value
      }
    })
}



////////////////////////////////////////////////////////////////
  handleSubmit(e) {
    e.preventDefault();

  // this.setState({loading:true})

    
   /*  firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log(user.email) */
            fetch(
              'https://us-central1-asignacion-tarifas.cloudfunctions.net/registerRegion',
              {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  regionName:this.state.value.regionName, 
                  available:this.state.value.available,
                  radioNotAvailable:this.state.value.radioNotAvailable,
                  uid: this.state.uid
                }),
              }).then(response=>{
                if(response.ok){
                  return response.json()
                }else{
                  throw new Error('Algo salio mal :(');
                }
              })
              .then(resp=>{
                // console.log('value-response:', resp.codeRegion)
                // console.log('value-response:', resp.success)
                // console.log(this.state.uid)
                if(resp.success===1){
                    toast.success(' Region creado correctamente!!!');
                    }else{
                      toast.error(' Region creado incorrectamente!!!');
                  }
                this.setState({regionsList:{ 
                  regionCode:resp.codeRegion,
                  regionName:this.state.value.regionName, 
                  available:this.state.value.available,
                  radioNotAvailable:this.state.value.radioNotAvailable, 
                  numberRepresentativePoint:0
                  
                  }
              },() => this.props.onRegions(this.state.regionsList)
                  )
                // console.log(this.state.regionsList)
                // console.log(this.state.uid)

                });
               
              
              // ).then(response=> response.json())
              // .then((json) => json.success)
              // .then(success =>{
              //   console.log('value-response:', success)
              //   if(success===1){
              //     toast.success(' Lugar creada correctamente!!!');
              //     }else{
              //       toast.error(' Lugar creada incorrectamente!!!')
              //     }
              //   });



               

        /* } else {
          console.log("No hay sesión activa");
        }
      });   */      
      
      // .catch((error) => {
      //   ToastsStore.error("Error. Inténtelo de nuevo más tarde.", {
      //     position: ToastsStore.POSITION.TOP_RIGHT
      //   });
      //   console.log("Ha ocurrido un error, intentelo más tarde!", error)
      // });
      // 

      /* 
      .catch(error => console.error('Error:', error))
      .then((json) => console.log(json))
   .then(resp=>{
    //   // console.log(resp)
    //   //Prueba de consola si se guarda
    //   console.log(this.state.value.regionName);
    //   console.log(this.state.value.available);
    //   console.log(this.state.value.radioNotAvailable);
    //   // console.log(resp.regionCode)
      // if (resp.success===1) { // exito
      //   alert('Lugares creada');
      // }
    //   // this.setState({regionsList: resp.regionsList})
  });
    */
}



////////////////////////////////////////////////////////////////
  componentWillMount(){
    firebase.auth().onAuthStateChanged(user=> {
      if (user) {
        // console.log(user.uid)
        var uid = user.uid;
        this.setState({uid:uid})
      } else {
        console.log("No hay sesión activa");
      }
    });
  }

/////////////////////////////////////////////////////////////////////////
    render() {

        const {loading}=this.state;
          if(loading){
            // return 'loading...';
          return // component
          } 


          // const enabled=this.state.value.available===1;
          // let inputNoDisponible;
          // if(this.state.value.available===0){
            // if(this.state.value.available==1){
            //   this.setState({value:{
            //     radioNotAvailable:'0'
            //   }})
            // };

            let inputNoDisponible = null;
            if(this.state.value.available==0){
                  console.log(this.state.value.available)

                  inputNoDisponible=
                  <Form.Group as={Row} controlId="formHorizontalRND">
                    <Form.Label column sm={3}>
                    Radio de no Disponibilidad
                    </Form.Label>
                    <Col sm={9}>
                    <Form.Control type="text" name="radioNotAvailable" value={this.state.value.radioNotAvailable} onChange={this.handleChange} placeholder="Ingrese el RND" />
                    </Col>
                  </Form.Group>
            }else {
              inputNoDisponible=null;
              // this.setState({value:{
              //   radioNotAvailable:''
              // }})
            }

          //Para validacion es basica
          const { regionName, available} = this.state.value;
          const enabled =
          regionName.length > 0 &&
          available.length > 0 ;

      return (
        <div>
        {/* <Loader type="Circles" color="#somecolor" height={40} width={40}/> */}


        <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
            />
            {/* Same as */}
        <ToastContainer />
        

        <Modal
          {...this.props}
          size=""
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Registrar Nuevo Lugar
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            
            <Form onSubmit={this.handleSubmit}>
                <Form.Group as={Row} controlId="formHorizontalnombre">
                    <Form.Label column sm={3}>
                    Nombre
                    </Form.Label>
                    <Col sm={9}>
                    <Form.Control type="text"  name="regionName" value={this.state.value.regionName.toUpperCase()} onChange={this.handleChange} placeholder="Ingrese el nombre" />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="SelectED">
                    <Form.Label column sm={3}>
                    Estado de Disponibilidad
                    </Form.Label>
                    <Col sm={9}>
                    <Form.Control as="select" name="available" value={this.state.value.available} onChange={this.handleChange}>
                    <option value="1">SI</option>
                    <option value="0">NO</option>
                    </Form.Control>
                    </Col>
                </Form.Group>
                 
                {inputNoDisponible}

                {/* <Form.Group as={Row} controlId="formHorizontalRND">
                    <Form.Label column sm={3}>
                    Radio de no Disponibilidad
                    </Form.Label>
                    <Col sm={9}>
                    <Form.Control type="text" name="radioNotAvailable" value={this.state.value.radioNotAvailable} onChange={this.handleChange} placeholder="Ingrese el RND" />
                    </Col>
                </Form.Group>  */}



                <Modal.Footer>
                    <Button variant="danger" onClick={this.props.onHide}>Cancelar</Button>
                    <Button variant="success" onClick={this.props.onHide} type="submit" value="Submit" disabled={!enabled}>Guardar</Button>
                </Modal.Footer>
                
            </Form>
          </Modal.Body>
          
        </Modal></div>
      );
    }
  }

  export default ModalNuevo;

 