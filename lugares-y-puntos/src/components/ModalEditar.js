import React from 'react';
import { Button, Modal, Form, Col, Row } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import firebase from 'firebase';

class ModalEditar extends React.Component {

  
 ////////////////////////////////////////////////// 
constructor(props){
  super(props);
  this.state={
    uid:'',
    regionCode:'',
    value:{
      regionName:'',
      available:'1',
      radioNotAvailable:'0',
      uid:'' 
    },
    regionsList:{
      regionCode:'',
      regionName:'',
      available:'',
      radioNotAvailable:'',
      numberRepresentativePoint: 0,
      uid:''
    }
  }

  this.handleChange = this.handleChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);
  // const regionCode = props.regionCodeEdit;
  this.funcionCode=this.funcionCode.bind(this)
  //console.log(this.props.regionCodeEdit)
}


componentWillMount(){
  firebase.auth().onAuthStateChanged(user=> {
    if (user) {
      // console.log(user.uid)
      var uid = user.uid;
      this.setState({uid:uid})
    } else {
      // console.log("No hay sesión activa");
    }
  });
}

/////////////////////////////////////////////////////////////////
handleChange = e =>{   
  this.setState({ 
    value:{
      ...this.state.value,
      [e.target.name] :e.target.value
    }
  })
}


////////////////////////////////////////////////////////////////
handleSubmit(e) {
  e.preventDefault();

  fetch(
    'https://us-central1-asignacion-tarifas.cloudfunctions.net/editRegion',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        regionCode: this.props.data.regionCode, 
        regionName:this.state.value.regionName, 
        available:this.state.value.available,
        radioNotAvailable:this.state.value.radioNotAvailable,
        uid:this.state.uid
      }),
    }
  )
  .then(response=>response.json())
  .then((json) => json.success)
  .then(success =>{
    // console.log('value-response:', success)
    // console.log(this.state.uid)
    if(success===1){
      toast.success(' Lugar editada correctamente!!!');
      }else{
        toast.error(' Lugar editada incorrectamente!!!')
      }
      this.setState({regionsList:{ regionCode:this.props.data.regionCode,regionName:this.state.value.regionName, available:this.state.value.available,radioNotAvailable:this.state.value.radioNotAvailable, numberRepresentativePoint:this.props.data.numberRepresentativePoint}},
        () => this.props.onRegionEdit(this.props.dataEditIndex,this.state.regionsList)
        )
    })


    
  // .then(resp=>{
  //   //Prueba de consola si se guarda
  //   console.log( this.props.data.regionCode);
  //   console.log(this.state.value.regionName);
  //   console.log(this.state.value.available);
  //   console.log(this.state.value.radioNotAvailable);
  //   // if (resp.success===1) { // exito
  //   //   alert('Lugares creada');
  //   // }
  //   // this.setState({regionsList: resp.regionsList})
  // })
  // .catch(error => console.error('Error:', error))
  // .then(response => console.log('Success:', response));

  

}
////////////////////////////////////////////////////////////////

funcionCode(){
  // this.setState({
  //   regionCode: this.props.regionCodeEdit},
  //    () =>   {console.log(this.props.regionCodeEdit)}
  //   )
  // console.log(this.props.regionCodeEdit)
}






//////////////////////////////////////////////////////////////////
    render() {

     // Para validacion es basica
      const { regionName, available, radioNotAvailable } = this.state.value;
          const enabled =
          regionName.length > 0 &&
          available.length > 0 &&
          radioNotAvailable.length >0;

      // Para habilitar de available
      let inputNoDisponible = null;
            if(this.state.value.available==0){
                  console.log(this.state.value.available)

                  inputNoDisponible=
                  <Form.Group as={Row} controlId="formHorizontalRND">
                    <Form.Label column sm={2}>
                    Radio de no Disponibilidad
                    </Form.Label>
                    <Col sm={10}>
                    <Form.Control type="text" placeholder={this.props.data.radioNotAvailable} name="radioNotAvailable" value={this.state.value.radioNotAvailable} onChange={this.handleChange}/>
                    </Col>
                </Form.Group>
            }else {
              inputNoDisponible=null;
              // this.setState({value:{
              //   radioNotAvailable:''
              // }})
            }

      return (

        <div>
        <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
            />
            {/* Same as */}
        <ToastContainer />

        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Editar Lugar ({this.props.data.regionCode})
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            
            <Form onSubmit={this.handleSubmit}>
                <Form.Group as={Row} controlId="formHorizontalnombre">
                    <Form.Label column sm={2}>
                    Nombre
                    </Form.Label>
                    <Col sm={10}>
                    <Form.Control type="text" placeholder={this.props.data.regionName} name="regionName" value={this.state.value.regionName} onChange={this.handleChange} />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="SelectED">
                    <Form.Label column sm={2}>
                    Estado de Disponibilidad
                    </Form.Label>
                    <Col sm={10}>
                    <Form.Control as="select" placeholder={this.props.data.available} name="available" value={this.state.value.available} onChange={this.handleChange}>
                    <option value="1">SI</option>
                    <option value="0">NO</option>
                    </Form.Control>
                    </Col>
                </Form.Group>

                {/* <Form.Group as={Row} controlId="formHorizontalRND">
                    <Form.Label column sm={2}>
                    Radio de no Disponibilidad
                    </Form.Label>
                    <Col sm={10}>
                    <Form.Control type="text" placeholder={this.props.data.radioNotAvailable} name="radioNotAvailable" value={this.state.value.radioNotAvailable} onChange={this.handleChange}/>
                    </Col>
                </Form.Group> */}
                {inputNoDisponible}

                <Modal.Footer>
                  <Button variant="danger" onClick={this.props.onHide}>Cancelar</Button>
                  <Button variant="success" onClick={this.props.onHide} disabled={!enabled} type="submit" value="Submit">Guardar</Button>
                </Modal.Footer>
            </Form>
          </Modal.Body>
          
        </Modal></div>
      );
    }
  }

  export default ModalEditar
  ;