import React from 'react';
import { Button, Modal, Form, Col, Row } from 'react-bootstrap';
// import Loading from './Loading';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import firebase from 'firebase';
import Loading from './Loading';


class ModalNuevoCodigo extends React.Component {

constructor(props){
  super(props);
   this.state={
  uid:'',
  name:'',
  loading: false,
  coordenada:'',
  
    value:{
      regionCode:'',
      latitud:'',
      longitud:'',
      uid:'',
        },

  representativePointsList:{
    pointCode:'',
    latitud:'',
    longitud:'',
    uid:''
  },
    };
  
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

}


/////////////////////////////////////////////////////////////////
handleChange = e =>{   
  this.setState({
      [e.target.name] :e.target.value
  },
  () => {
    let coord = this.state.coordenada
    // console.log(coord)
    let secuencia = coord.split(", ")
    // console.log(secuencia[0])
    // console.log(secuencia[1])

    this.setState({
      value:{
        ...this.state.value,
        regionCode:this.props.regionCodeData,
        latitud:secuencia[0],
        longitud:secuencia[1],
        uid:this.state.uid
      }
    }, 
    // () =>   console.log(this.state.value)
    )
  }
  )



}


// handleChange = e =>{   
//   this.setState({ 
//     value:{
//       ...this.state.value,
//       [e.target.name] :e.target.value
//     }
//   })
// }

////////////////////////////////////////////////////////////////
handleSubmit(e) {
  e.preventDefault();
  
  // this.setState({loading:true})


  fetch(
    'https://us-central1-asignacion-tarifas.cloudfunctions.net/registerRepresentativePoint',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        regionCode:this.props.regionCodeData, 
        latitud:this.state.value.latitud, 
        longitud:this.state.value.longitud,
        uid:this.state.uid
      }),
    }
  ).then(response=>{
    
    if(response.ok){
      return response.json()
    }else{
      throw new Error('Algo salio mal :(');
    }
  })
  .then(resp=>{
    // console.log('value-response:', resp.codeRepresentativePoint)
    // console.log('value-response:', resp.success)
    // console.log(this.state.uid)
    // console.log(this.props.regionCode)

    if(resp.success===1){
        // this.setState({loading:false})
        toast.success(' Punto representativo creado correctamente!!!');
        }else{
          toast.error(' Punto representativo creado incorrectamente!!!');
      }
    this.setState({
      representativePointsList:{ 
        pointCode:resp.codeRepresentativePoint,
        latitud:this.state.value.latitud,
        longitud:this.state.value.longitud,
        }})
    // console.log(this.state.representativePointsList)
    // console.log(this.state.value2.coordenada)
    // this.handleLoad();


      // if(resp.codeRepresentativePoint!==''){
         this.props.onPoint(this.state.representativePointsList)
      //  }
   

  });
  
  
 }




componentWillMount(){
  firebase.auth().onAuthStateChanged(user=> {
    if (user) {
      // console.log(user.uid)
      var uid = user.uid;
      this.setState({uid:uid})
    } else {
      // console.log("No hay sesión activa");
    }
  });

}



    render() {

  

       //Para Cargar Loading antes de ejecutar de datos de servicio web
       const {loading, error}=this.state;

       if(error){
         return <p>{error.message}</p>
       }
   
       if(loading){
         // return 'loading...';
        return <Loading/>
       } 


      // Para validacion es basica
      const {coordenada} = this.state;
          const enabled = coordenada.length > 0;
    
      
      return (

        <div>
        <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
            />
            {/* Same as */}
        <ToastContainer />


        <Modal
          {...this.props}
          size=""
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            Registrar Nuevo Punto Representativo
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            
            <Form onSubmit={this.handleSubmit}>
                <Form.Group as={Row} controlId="formHorizontalRND">
                    <Form.Label column sm={3}>
                    Coordenada:
                    </Form.Label>
                    <Col sm={9}>
                    <Form.Control type="text" name="coordenada" value={this.state.coordenada} onChange={this.handleChange}  placeholder="Ingrese la coordenada"/>
                    </Col>
                </Form.Group>

          <Modal.Footer>
            <Button variant="danger" onClick={this.props.onHide}>Cancelar</Button>
            <Button variant="success" type="submit" value="Submit" onClick={this.props.onHide} disabled={!enabled} >Guardar</Button>
          </Modal.Footer>

            </Form>
          </Modal.Body>
        </Modal> 

        </div>

      );
    }
  }

  export default ModalNuevoCodigo;