import React from 'react';
import { Spinner} from 'react-bootstrap';
import './styles/PageLoading.css';


export default class Loading  extends React.Component {
  render() {
    return (
      <div className="PageLoading">
         <Spinner animation="border" variant="info" />

      </div>
    );
  }
}