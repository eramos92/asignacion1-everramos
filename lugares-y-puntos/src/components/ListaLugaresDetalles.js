import React from 'react';

import './styles/ListaLugares.css';
import { Button, ButtonToolbar} from 'react-bootstrap';
import ModalNuevoCodigo from './ModalNuevoCodigo';
import ModalEliminarPunto from './ModalEliminarCodigo';
import Loading from './Loading'

import firebase from 'firebase';


//Mostrar la vista de listado, busqueda y agregar
class ListaLugaresDetalles extends React.Component {

  //////////////////////////////////////////////////////////////////////////////////////////////////////
  //Para no abrir la ventana despues de actualizar la carga OBS: debe ser seleccionado el button agregar
  constructor(props,{match}) {
    super(props,{match});

    this.state = { 
      modalShow: false, 
      representativePointsList:[],
      modalShow1: false,
      loading: false,
      regionToNew:'',
      pointToDelete:{},
      pointToDeleteIndex:{} 
    };

    this.handleOnAddRepresentativePoint = this.handleOnAddRepresentativePoint.bind(this);
    this.handleOnDeleteRepresentativePoint = this.handleOnDeleteRepresentativePoint.bind(this);


  }

  

  /////////////////////////////////////////////////////////////////////////////////////////////////
  //Para cargar los puntos de representativos
  // Para mostrar la consola si el usuario esta activo o no
  componentDidMount(){

    // console.log(this.props.match.params.regionCode)


    this.setState({loading:true})
 
    // this.intervalId = setInterval(() =>
    fetch(
      'https://us-central1-asignacion-tarifas.cloudfunctions.net/getRepresentativePoints',
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({regionCode:this.props.match.params.regionCode}),
        
      }
    ).then(response=>{
      
      if(response.ok){
        return response.json()
      }else{
        throw new Error('Algo salio mal :(');
      }
    })
    .then(resp=>{
      this.setState({representativePointsList: resp.representativePointsList,loading:false}); 
      this.setState({regionToNew:this.props.match.params.regionCode})
      // console.log(resp.representativePointsList)
    }).catch(error=>this.setState({error,loading:false}))

    

    
    // , 3000);

    // console.log(this.state.representativePointsList)

    // firebase.auth().onAuthStateChanged(function(user) {
    //   if (user) {
    //     console.log(user)
        
    //     var displayName = user.displayName;
    //     var email = user.email;
    //     var emailVerified = user.emailVerified;
    //     var photoURL = user.photoURL;
    //     var isAnonymous = user.isAnonymous;
    //     var uid = user.uid;
    //     var providerData = user.providerData;
    //     // ...
    //   } else {
    //     // User is signed out.
    //     // ...
    //   }
    // });
  }


  //////////////////////////////////////////////////////////////////////////////////////
  //Para agregar objeto dentro arreglo representativePointsList sin la carga
  handleOnAddRepresentativePoint(objeto){
    let list = this.state.representativePointsList
    list.unshift(objeto)
    this.setState({
      representativePointsList: list
    })
  }
  

  ///////////////////////////////////////////////////////////////////////////////////
  //Para eliminar objeto dentro arreglo representativePointsList sin la carga
  handleOnDeleteRepresentativePoint(objeto){
     
    let listDelete = this.state.representativePointsList
    listDelete.splice(objeto,1)
    this.setState({
      representativePointsList: listDelete
    })
  }

  
  /////////////////////////////////////////////////////////////////////////////////////
  componentWillMount() {
    firebase.auth().onAuthStateChanged(user=> {
      if (!user) {
        // console.log(`No tiene sesion en este Detalles: ${user} `);
        this.props.history.push('/');
      }else{
        // console.log(`Tiene sesion en este Detalles: ${user.email} `)
  
      }
    });
    
  }

  componentWillUnmount(){
    // console.log("Salí de la vista de detalles")
  }
  

    //////////////////////////////////////////////////////////////////////////////////////////////
    render() {

        //Para Cargar Loading antes de ejecutar de datos de servicio web
        const {loading, error}=this.state;

        if(error){
          return <p>{error.message}</p>
        }
    
        if(loading){
          // return 'loading...';
         return <Loading/>
        } 

      var messageRepresentativePointsList;
      if (this.state.representativePointsList.length===0) {
        messageRepresentativePointsList = <p style={messageStyle}>NO HAY DATOS LOS PUNTOS REPRESENTATIVOS DE {this.props.match.params.regionName}</p>;
        // console.log('no hay datos')
      }

      // this.handleOnAddRepresentativePoint();

      //console.log(this.state.regionToNew)
      
      //Para cerrar la ventana en X de modal nuevo
      let modalClose = () => this.setState({ modalShow: false });
      let modalClose1 = () => this.setState({ modalShow1: false });


    
  
    

      return (
      
      <div className="Lista__container">
      
        <div className="container">
        <p style={regionNameStyle}>LOS PUNTOS REPRESENTATIVOS DE {this.props.match.params.regionName}</p>
          <ButtonToolbar>
                          <Button
                            variant="success"
                            onClick={() => this.setState({ modalShow: true })}
                            className="btn mb-3"
                          >
                            Agregar
                          </Button>

                          <ModalNuevoCodigo
                            onPoint={this.handleOnAddRepresentativePoint}
                            show={this.state.modalShow}
                            onHide={modalClose}
                            regionCodeData={this.state.regionToNew}
                          />
          </ButtonToolbar>
          
          {/* Comienza la tablas de puntos representativos de colonias*/}
          <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">Código de Registro  </th>
              <th scope="col">Latitud</th>
              <th scope="col">Longitud</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
          <tbody>
          {messageRepresentativePointsList}
          {this.state.representativePointsList.map((punto, index)=>{
            return (
              <tr key={punto.pointCode}>
              <th scope="row">{punto.pointCode}</th>
              <td>{punto.latitud}</td>
              <td>{punto.longitud}</td>
              <td> 
               
              {/* Para eliminar */}
              <ButtonToolbar>
                          <Button
                            variant="danger"
                            onClick={() => this.setState({ modalShow1: true, pointToDelete:punto, pointToDeleteIndex:index})}
                            className="btn btn-danger"
                          >
                            Eliminar 
                          </Button>

              </ButtonToolbar> 
              </td>
            </tr>
            )
          })}
          </tbody>
        </table>
        <ModalEliminarPunto
          onPointDelete={this.handleOnDeleteRepresentativePoint}
          show={this.state.modalShow1}
          onHide={modalClose1}
          dataDelete={this.state.pointToDelete}
          dataDeleteIndex={this.state.pointToDeleteIndex}
        />
        </div>   
          
      </div>
      );
      
  }

  
} 
export default ListaLugaresDetalles;


/////////////////////////////////////////STYLES/////////////////////////
const messageStyle = {
  textAlign: 'center',
  margin:'120px',
  fontWeight: 'bold',
  fontSize: 20,
};

const regionNameStyle={
  textAlign: 'center',
  fontWeight: 'bold',
  fontSize: 20,

  
}