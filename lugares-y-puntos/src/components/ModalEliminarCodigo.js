import React from 'react';
import { Button, Modal} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class ModalEliminarPunto extends React.Component {

  constructor(props){
    super(props);
    this.state={
    };

    this.handleDeletePoint = this.handleDeletePoint.bind(this);

  }


  handleDeletePoint(){
    
    fetch(
      'https://us-central1-asignacion-tarifas.cloudfunctions.net/removeRepresentativePoint',
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({codeRepresentativePoint:this.props.dataDelete.pointCode}),
      }
      ).then(response=> response.json())
      .then((json) => json.success)
      .then(success =>{
        // console.log('value-response:', success)
        if(success===1){
          // console.log('eliminado')
            toast.success(' Punto Representativo esta eliminado!!!');
          }else{
            toast.error(' Error al eliminar punto representativo!!!')
          }
        });

        this.props.onPointDelete(this.props.dataDeleteIndex)

        this.props.onHide()
  }


    render() {

      // let modalClose = () => this.setState({ modalShow: false });

      // console.log(this.props.dataDelete.pointCode)
      return (
        <div>
          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
            />
            {/* Same as */}
        <ToastContainer />
        

        <Modal
          {...this.props}
          size="mx"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              ¿Estás seguro?
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
                Desea Eliminar código de registro {this.props.dataDelete.pointCode}.           
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={this.handleDeletePoint}>Eliminar</Button>
            <Button variant="primary" onClick={this.props.onHide}>Cancel</Button>
          </Modal.Footer>
        </Modal></div>
      );
    }
  }

  export default ModalEliminarPunto;