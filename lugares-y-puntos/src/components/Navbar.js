import React from 'react';

import logo from '../images/logo_3.png';
import usuariologin from '../images/usuario.png';
import './styles/Navbar.css'
import {Link} from 'react-router-dom'
import firebase from 'firebase';

class Navbar extends React.Component{

    constructor(props) {
        super(props);

        this.state={
          email:''
        }
        //Para salir de usuario autenticado
        this.logout = this.logout.bind(this);
    }

    componentWillMount(){
    firebase.auth().onAuthStateChanged(user=> {
      if (user) {
          var email = user.email;
          this.setState({email:email})
        } else {
          // console.log("No hay sesión activa");
        }
      });
    }

    logout() {
        firebase.auth().signOut();
    }
    render(){

      var avatar;
      if(this.state.email){
        avatar=<a href="/" className="Avatar__usuario" onClick={this.logout}><img className="Avatar__usuario" src={usuariologin}  alt="Avatar"></img></a>
      } 


        return (
            <div className="Navbar">
                <div className="container-fluid">

                <div className="Navbar__brand">
                        <Link to='/home'><img className="Navbar__brand-logo" src={logo} alt="logo"/></Link>
                        <span className="Navbar__size font-weight-bold">Lugares y Puntos</span>
                        <p className="Correo__usuario">{this.state.email}</p>
                        {avatar}
                    </div>
                </div>
            </div>
        )
    }
}

export default Navbar;
