import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';


import 'bootstrap/dist/css/bootstrap.css';
//import 'materialize-css/dist/css/materialize.css';
import firebase from 'firebase';


//Firebase de autenticaion debe estar en privado de no subir GIT
firebase.initializeApp({
    apiKey: "AIzaSyCdDQoMJXf5NpNL-gAczAAPZwCIXHzZHUI",
    authDomain: "lugares-y-puntos.firebaseapp.com",
    databaseURL: "https://lugares-y-puntos.firebaseio.com",
    projectId: "lugares-y-puntos",
    storageBucket: "lugares-y-puntos.appspot.com",
    messagingSenderId: "918300657306",
    appId: "1:918300657306:web:8d5894ea9550c254"
});


const contenedor = document.getElementById('app');

ReactDOM.render(<App />, contenedor);
